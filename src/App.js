import React from 'react'
import logo from './logo.svg'
import './App.css'

function Random() {
  let [random, setRandom] = React.useState()

  function onClick() {
    fetch(
      'https://llps1sry9b.execute-api.eu-central-1.amazonaws.com/dev/random'
    )
      .then(a => a.json())
      .then(a => setRandom(Math.floor(a.number * 1000)))
  }

  return (
    <div>
      <button onClick={onClick}>
        {random === undefined ? 'fetch random number' : random}
      </button>
    </div>
  )
}

function App() {
  return (
    <div className='App'>
      <header className='App-header'>
        <img src={logo} className='App-logo' alt='logo' />
        <Random />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <div>hope it works now</div>
        <a
          className='App-link'
          href='https://reactjs.org'
          target='_blank'
          rel='noopener noreferrer'
        >
          Learn React
        </a>
      </header>
    </div>
  )
}

export default App
